package ru.renessans.jvschool.volkov.tm.api;

import ru.renessans.jvschool.volkov.tm.enumeration.CommandType;

public interface ICommandRepository {

    String getNotifyByCommandType(final CommandType commandType, final String command);

}