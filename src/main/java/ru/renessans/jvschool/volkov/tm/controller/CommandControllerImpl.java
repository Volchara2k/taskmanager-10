package ru.renessans.jvschool.volkov.tm.controller;

import ru.renessans.jvschool.volkov.tm.api.ICommandController;
import ru.renessans.jvschool.volkov.tm.api.ICommandService;
import ru.renessans.jvschool.volkov.tm.constant.NotifyConst;

public final class CommandControllerImpl implements ICommandController, NotifyConst {

    private final ICommandService commandService;

    public CommandControllerImpl(final ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void printCmdNotify(final String command) {
        System.out.println(this.commandService.getCmdNotify(command));
    }

    @Override
    public void printArgNotify(final String command) {
        System.out.println(this.commandService.getArgNotify(command));
    }

}