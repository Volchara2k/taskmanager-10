package ru.renessans.jvschool.volkov.tm.bootstrap;

import ru.renessans.jvschool.volkov.tm.api.ICommandController;
import ru.renessans.jvschool.volkov.tm.api.ICommandRepository;
import ru.renessans.jvschool.volkov.tm.api.ICommandService;
import ru.renessans.jvschool.volkov.tm.constant.CmdConst;
import ru.renessans.jvschool.volkov.tm.controller.CommandControllerImpl;
import ru.renessans.jvschool.volkov.tm.repository.CommandRepositoryImpl;
import ru.renessans.jvschool.volkov.tm.service.CommandServiceImpl;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.Scanner;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepositoryImpl();

    private final ICommandService commandService = new CommandServiceImpl(commandRepository);

    private final ICommandController commandController = new CommandControllerImpl(commandService);

    public void run(String... args) {
        if (ValidRuleUtil.isNullOrEmpty(args)) {
            commandPrintLoop();
        } else {
            argPrint(args);
        }
    }

    private void commandPrintLoop() {
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!CmdConst.EXIT.equals(command)) {
            command = scanner.nextLine();
            this.commandController.printCmdNotify(command);
        }
    }

    private void argPrint(String... args) {
        final String arg = args[0];
        this.commandController.printArgNotify(arg);
    }

}