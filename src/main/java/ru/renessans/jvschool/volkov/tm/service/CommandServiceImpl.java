package ru.renessans.jvschool.volkov.tm.service;

import ru.renessans.jvschool.volkov.tm.api.ICommandRepository;
import ru.renessans.jvschool.volkov.tm.api.ICommandService;
import ru.renessans.jvschool.volkov.tm.constant.NotifyConst;
import ru.renessans.jvschool.volkov.tm.enumeration.CommandType;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.Objects;

public final class CommandServiceImpl implements ICommandService, NotifyConst {

    private final ICommandRepository commandRepository;

    public CommandServiceImpl(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public String getCmdNotify(final String command) {
        if (ValidRuleUtil.isNullOrEmpty(command))
            return NO_COMMAND_MSG;
        return notificationByCommandType(CommandType.CMD, command);
    }

    @Override
    public String getArgNotify(final String command) {
        if (ValidRuleUtil.isNullOrEmpty(command))
            return NO_COMMAND_MSG;
        return notificationByCommandType(CommandType.ARG, command);
    }

    private String notificationByCommandType(final CommandType commandType, final String command) {
        if (Objects.isNull(commandType)) {
            return NO_COMMAND_TYPE_MSG;
        }
        if (ValidRuleUtil.isNullOrEmpty(command))
            return NO_COMMAND_MSG;

        String notify = "";
        if (commandType.isArg())
            notify = commandRepository.getNotifyByCommandType(CommandType.ARG, command);
        if (commandType.isCmd())
            notify = commandRepository.getNotifyByCommandType(CommandType.CMD, command);
        if (ValidRuleUtil.isNullOrEmpty(notify))
            return String.format(FORMAT_MSG_UNKNOWN, command);
        return notify;
    }

}