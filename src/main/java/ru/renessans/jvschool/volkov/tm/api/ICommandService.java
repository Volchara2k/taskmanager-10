package ru.renessans.jvschool.volkov.tm.api;

public interface ICommandService {

    String getCmdNotify(final String command);

    String getArgNotify(final String command);

}