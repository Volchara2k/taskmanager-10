package ru.renessans.jvschool.volkov.tm.api;

public interface ICommandController {

    void printCmdNotify(final String command);

    void printArgNotify(final String command);

}