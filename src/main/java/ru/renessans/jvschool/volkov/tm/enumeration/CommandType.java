package ru.renessans.jvschool.volkov.tm.enumeration;

public enum CommandType {

    CMD, ARG;

    public boolean isCmd() {
        return this == CMD;
    }

    public boolean isArg() {
        return this == ARG;
    }

}